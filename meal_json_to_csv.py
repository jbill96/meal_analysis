import json
import csv
import sys

'''
adds '~' between ingredient elements

converts [{text : ''},...]

to "<string>~<string>~..."

use <string>.split('|') to get list of string elements
'''
def concat_dict_list(dict_list, key='text'):
	ing_str = ''
	length = len(dict_list)
	for i in range(length):
		ing = dict_list[i][key]
		ing = str(ing)
		if (i != (length - 1)):
			ing += '|'
		ing_str += ing
	return ing_str

'''
main parsing function to turn json file into a more csv like file
'''
def parse_file(input_file, output_file):
	columns = ['title', 'ingredients', 'instructions', 'id', 'partition', 'url']
	with open(output_file, 'w') as out_file:
		writer = csv.writer(out_file)
		writer.writerow(columns)
		with open(input_file, 'r') as f:
			data = json.load(f)
			for meal in data: 
				meal_row = []
				ingredients = meal['ingredients']
				ingredients = concat_dict_list(ingredients)

				instructions = meal['instructions']
				instructions = concat_dict_list(instructions)

				title = meal['title']
				url = meal['url']
				partition = meal['partition']
				id = meal['id']

				meal_row = [title, ingredients, instructions, id, partition, url]
				writer.writerow(meal_row)
				

if __name__ == "__main__":
	input_file = str(sys.argv[1])
	output_file = str(sys.argv[2])

	if not(input_file or output_file):
		print('please input two file paths 1.) input file 2.) output file')
	else :
		parse_file(input_file, output_file)
